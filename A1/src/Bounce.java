/*  Jasper Yun - 260651891
 *  ECSE202 - Assignment 1
 *  
 *  Much of the code in this program comes from the assignment instructions.
 *  This program simulates the projectile motion of a ball of mass 1 kilogram, launched at an angle theta and initial velocity
 *  V0 chosen by user input. The simulation accounts for air resistance.
 */

import acm.graphics.*;
import acm.program.GraphicsProgram;

import java.awt.Color;

public class Bounce extends GraphicsProgram 
{
	// ---------  PARAMETERS:  ---------------------------------- //
	// declare parameters -- from assignment instructions
	private static final int WIDTH = 600;  // pixels -- width of screen canvas
	private static final int HEIGHT = 600; // pixels -- distance from top of screen to ground plane
	private static final int OFFSET = 200; // pixels -- distance from ground to bottom of screen

	
	private static final double g = 9.8;   			// gravitational constant on Earth
	private static final double Pi = 3.141592654;	// used for converting degrees to radians
	
	// m, k do not change in our program, so change to parameters
	private static final double m = 1.0; 			// mass of ball, given in assignment instructions
	private static final double k = 0.0016; 		// used for air resistance
	
	
	private static final double Xinit = 5.0;		// initial X position in screen coordinates
	private static final double Yinit = 0.0;		// initial Y position, in screen coordinates

	
	private static final double TICK = 0.1;			// time increment value	 
	private static final double ETHR = 0.01;		// threshold for termination: if KEx or KEy < ETHR then STOP
	private static final double PD = 1;				// diameter of tracking points (pixels)
	
	
	private static final double XMAX = 100.0;		// maximum X value in metres
	private static final double YMAX = 100.0;		// maximum Y value in metres
	private static final double SCALE = WIDTH/XMAX;	// scale factor in pixels/metre

	
	private static final boolean TEST = false; 		// for testing, print info if test true
	
	
	public void run() 
	{
		this.resize(WIDTH,HEIGHT+OFFSET);	// resize the canvas
		
		// ensuring proper inputs, i.e. prevent inputs outside of range specified in console dialog
		double V0 = -5; // set V0 to wrong value, then use while loop to ensure an input in [0, 100]
		while (V0 < 0 || V0 > 100)
		{
			V0 = readDouble("Enter the initial velocity of the ball in m/s [0, 100]: ");
		}
		
		double theta = -5;
		while (theta < 0 || theta > 90)
		{
			theta = readDouble("Enter the launch angle in degrees [0, 90]: ");
		}
		
		double loss = -5;
		while (loss < 0 || loss > 1) 
		{
			loss = readDouble("Enter the energy loss parameter [0, 1]: ");
		}
		
		double bSize = -5;
		while (bSize < 0.1 || bSize > 5.0)
		{
			bSize = readDouble("Enter the radius of the ball in metres [0.1, 5.0]: ");
		}
		
		
		
		// ---------  GRAPHICS:  -------------------------------- //
		// draw graphics display and ball:
		GRect ground = new GRect(0,HEIGHT,WIDTH,3);
		ground.setFilled(true);
		ground.setFillColor(Color.BLACK);
		
		// place ball at (Xinit, Yinit) coordinates initially
		// recall that bSize is the *radius* of the ball in metres, so transform it to pixels
		GOval ball = new GOval(Xinit, Yinit, 2 * bSize * SCALE, 2 * bSize * SCALE);
		ball.setFilled(true);
		ball.setFillColor(Color.RED);	// fill color = red
		ball.setColor(Color.RED); 		// make border of ball red as well
		
		// add ground and ball to canvas
		add(ground);
		add(ball);
		
		
		// ---------  INITIALIZE VARIABLES:  -------------------- //
		
		double V0x = V0 * Math.cos(theta * Pi/180); 	// X component of initial velocity V0
		double V0y = V0 * Math.sin(theta * Pi/180); 	// Y component of initial velocity V0
		double Vt = m * g / (4 * Pi * bSize * bSize * k); // terminal velocity of ball
		
		double Xlast = 0; 		// last position of X, Y are Xinit, Yinit respectively
		double Ylast = bSize; 	// we place these two initializations after initializing X to output proper results
		
		double X = Xinit; 		// X begins at Xinit
		double Y = Yinit; 		// Y begins at Yinit
		double Xoff = Xinit; 	// offset of X which gets changed every bounce
		
		double Vx = V0x; 		// velocity in x direction will be same as initial x velocity at t = 0 
		double Vy = V0y; 		// velocity in y direction will be same as initial y velocity at t = 0
		
		double KEx = 0.5 * Vx * Vx * (1 - loss); // kinetic energies as given by assignment instructions
		double KEy = 0.5 * Vy * Vy * (1 - loss);
		
		double t = 0;			// start time t=0
		double totalTime = 0;	// variable to track total time elapsed, used in print statement of testing
		
		
		// ---------  MAIN LOOP:  ------------------------------- //
		// loop for ball simulation: runs until kinetic energy KEx or KEy < ETHR
		while (true) 
		{
			
			X = V0x * Vt / g * (1 - Math.exp(-g * t / Vt)); 							// update X position
			Y = bSize + Vt / g * (V0y + Vt) * (1 - Math.exp(-g * t / Vt)) - Vt * t; 	// update Y position
			
			/*  we need the Vx, Vy to be updated before we determine collision so that if there
			 *  is a collision, it is using the correct values for Vx, Vy in the calculation of
			 *  kinetic energies. Otherwise, he ball rolls at the end for a long time.
			 */
			
			Vx = (X - Xlast)/TICK; // update (estimate) x, y velocities based on displacement over time difference
			Vy = (Y - Ylast)/TICK; 
			
			
			// ---------  COLLISION DETECTION:  ----------------- //
			if(Y<=bSize && Vy<0)
			{
			KEx = 0.5 * Vx * Vx * (1 - loss); // calculate new kinetic energies in x, y directions after collision
			KEy = 0.5 * Vy * Vy * (1 - loss);
			
			V0x = Math.sqrt(2 * KEx); // calculate new initial x, y velocities from KEx, KEy
			V0y = Math.sqrt(2 * KEy);
			
			Xoff += X;  	// add current X to Xoff, but in METRE coordinates; using Xoff = X did not work
			t = 0; 			// reset time to 0
			X = 0; 			// reset X to 0 -- in position of ball, use Xoff + X
			Y = bSize;  	// ball is on ground, so Y = bSize
			Xlast = 0;  	// Xlast = X, and X = 0, so set Xlast = 0
			Ylast = bSize; 	// Ylast = Y and Y = bSize

			Vx = V0x; // re-initialize Vx, Vy since V0x, V0y were changed by collision
			Vy = V0y;
			}
			
			Xlast = X; // let Xlast be the current X coordinate (metres) of the ball
			Ylast = Y; // let Ylast be the current Y coordinate (metres) of the ball
			
			
			
			// ---------  DISPLAY UPDATE:  ---------------------- //
			int ScrX = (int) ((X + Xoff) * SCALE); 		  // changing X position to screen coordinates
			int ScrY = (int) (600 - (Y + bSize) * SCALE); // changing Y position to screen coordinates based on formula
			
			
			// if ball goes to the edge of the screen in the x direction, stop the program:
			// if (ScrX + 2 * bSize * SCALE > WIDTH) break;
			
			// tracking points of the ball:
			GOval points = new GOval(ScrX + SCALE * bSize, ScrY + SCALE * bSize, PD, PD);
			points.setFilled(true);
			points.setFillColor(Color.BLACK);

			add(points); // add tracking points

			ball.setLocation(ScrX, ScrY); // update location of ball
			
			
			if (TEST) // printing information for A1-1, A1-2 and debugging
			{
				System.out.printf("t: %.2f X: %.2f Y: %.2f Vx: %.2f Vy: %.2f\n", totalTime, Xoff + X, Y - bSize, Vx, Vy);
				
			}

			if ((KEx <= ETHR) || (KEy <= ETHR)) break; // condition to terminate program
			
			t += TICK; 				// increment time by TICK
			totalTime += TICK;		// increment total time by TICK
			pause(17);				// pause every 17 ms to simulate a natural-looking bounce
				
		}		
	}
}

