
/*  Jasper Yun - 260651891
 *  ECSE202 - Assignment 5
 *  
 *  Much of the code in this program comes from the assignment 
 *  instructions for Assignment 1 written by Prof. Ferrie. 
 *	The code was for assignment 1, but it has been modified 
 *  to work in "C" instead of Java.
 *  
 *  This program simulates the projectile motion of a ball of mass 1 kg,
 *  launched at an angle theta and initial velocity
 *  V0 chosen by user input. The simulation accounts for air resistance.
 *  
 *  Author: Jasper Yun
 *  Date: Novembr 18, 2019
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>       // for math functions (sin(), cos(), exp())

/* This code was written by Roey Borsteinas to deal with
   compiler errors that arose when this program was compiled
   on different platforms, e.g. Windows or Unix.
   The Windows compiler refused to compile scanf due to security
   issues and scanf_s isn't defined in Unix.
 */

#ifdef _WIN32
#undef scanf
#define scanf(format_string, ref) \
	scanf_s(format_string, ref)
#endif

// ---------  PARAMETERS:  ---------------------------------- //

#define g 9.8               // gravitational constant
#define Pi 3.1415926535     // Pi -- for converting radians to degrees

#define false 0
#define true !false

#define m 1.0               // mass of ball
#define k 0.0016            // air resistance of ball
#define Xinit 5.0	    	// initial X position in screen coordinates

#define TICK 0.1    		// time increment value
#define ETHR 0.01	    	// program termination if KEx or KEy < ETHR

/*
 * Main function of the program. This function contains the code
 * to run the simulation properly. The actual simulation is a 
 * while(true) condition to ensure the simulation runs
 * until the balls do not have sufficient energy to continue
 * bouncing. At that point, the loop breaks and the program terminates.
 * The code updates the position of the balls based on Newtonian mechanics.
 *
 */

void main(void)
{
	/*  ensuring proper inputs, i.e. prevent inputs outside of range 
        pecified in console dialog; we set parameters to wrong values,
        then use while loop to ensure proper input
	*/

	double V0 = -5, theta = -5, loss = -5, bSize = -5;
	while (V0 < 0 || V0 > 100)
	{
		printf("Enter the initial velocity of the ball in m/s [0, 100]: ");
        scanf(" %lf", &V0);
	}
		
	while (theta < 0 || theta > 90)
	{
		printf("Enter the launch angle in degrees [0, 90]: ");
        scanf("%lf", &theta);
	}
		
	while (loss < 0 || loss > 1) 
	{
		printf("Enter the energy loss parameter [0, 1]: ");
        scanf("%lf", &loss);
	}
		
	while (bSize < 0.1 || bSize > 5.0)
	{
		printf("Enter the radius of the ball in metres [0.1, 5.0]: ");
        scanf("%lf", &bSize);
	}
		
		
	// ---------  INITIALIZE VARIABLES:  -------------------- //
		
	double V0x = V0 * cos(theta * Pi/180); 	// X component of V0
	double V0y = V0 * sin(theta * Pi/180); 	// Y component of  V0
	double Vt = m * g / (4 * Pi * bSize * bSize * k); // terminal velocity
		
	double Xlast = 0; 		// last position of X, Y are Xinit, Yinit
	double Ylast = bSize;	// (Yinit = bSize)
		
	double X = Xinit; 		// X begins at Xinit
	double Y = bSize; 		// Y begins at bSize
	double Xoff = Xinit; 	// offset of X which gets changed every bounce
		
	double Vx = V0x; 		// Vx equals initial x velocity at t = 0
	double Vy = V0y; 		// Vy equals initial y velocity at t = 0
		
	double KEx = 0.5 * Vx * Vx * (1 - loss); // kinetic energies in x, y
	double KEy = 0.5 * Vy * Vy * (1 - loss);
		
	double t = 0;			// start time t=0
	double totalTime = 0;	// total time elapsed in simulation
		
		
	// ---------  MAIN LOOP:  ------------------------------- //
	// ball simulation loop runs until kinetic energy KEx or KEy < ETHR
	while (true) 
	{
		// update X and Y positions
		X = V0x * Vt / g * (1 - exp(-g * t / Vt)); 						
		Y = bSize + Vt / g * (V0y + Vt) * (1 - exp(-g * t / Vt)) - Vt * t;
			
		/*  we need the Vx, Vy to be updated before we determine collision
            so that if there is a collision, it is using the correct values 
            for Vx, Vy in the calculation of kinetic energies. Otherwise,
            the ball rolls at the end for a long time.
		*/
			
		Vx = (X - Xlast) / TICK; // update (estimate) x, y velocities based 
		Vy = (Y - Ylast) / TICK; // on difference of displacement over time
		
		// printing to console
		printf("t: %.2f X: %.2f Y: %.2f Vx: %.2f Vy: %.2f\n"
			, totalTime, Xoff + X, Y - bSize, Vx, Vy);

			
		// ---------  COLLISION DETECTION:  ----------------- //
		if(Y <= bSize && Vy < 0)
		{
			// new kinetic energies after collision:
            KEx = 0.5 * Vx * Vx * (1 - loss);
            KEy = 0.5 * Vy * Vy * (1 - loss);
                
			// calculate new initial x, y velocities from KEx, KEy
            V0x = sqrt(2 * KEx); 
            V0y = sqrt(2 * KEy);
                
            Xoff += X;  	// add current X to Xoff in METRE coordinates
            t = 0; 			// reset time to 0
            X = 0; 			// reset X to 0 -- use Xoff + X for X position
            Y = bSize;  	// ball is on ground, so Y = bSize
            Xlast = 0;  	// Xlast = X, and X = 0, so set Xlast = 0
            Ylast = bSize; 	// Ylast = Y and Y = bSize

			// re-initialize Vx, Vy since V0x, V0y are changed by collision
            Vx = V0x;
            Vy = V0y;

			printf("bounce\n");			// print to console    
        }
			
		Xlast = X; // let Xlast, Ylast be current positions of the ball
		Ylast = Y; // in metre coordinates (simulation units)
		
		if ((KEx <= ETHR) || (KEy <= ETHR)) 
		{
			// message indicating end of simulation
			printf("\nSimulation terminated.\n");
			break;
        }

		t += TICK;
		totalTime += TICK;
	}		
}