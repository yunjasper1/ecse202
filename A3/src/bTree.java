
/**
 * Jasper Yun - 260651891 <p>
 * ECSE202 Fall 2019 - Assignment 3 <p>
 * 
 * This class implements a non-recursive binary tree. <p>
 * 
 * The code in this class is heavily borrowed from the 
 * example class posted by Prof. Ferrie on myCourses for
 * ECSE202, Fall 2019. I have simply modified it to be able
 * to store aBall objects instead of integers. I have also
 * written methods required for assignment 3. <p>
 * 
 * @author Jasper Yun, ferrie
 * @date October 31, 2019
 *
 */

public class bTree 
{
	
	// Instance variables
	
    private bNode root=null;		// initialize empty bTree
    private double lastSize = 0;	// initialize lastSize = 0 to ensure that first ball gets placed
    private double X=0, Y=0;		// for stackBalls method; 
    private double DELTASIZE = 0.1;	// threshold to determine whether to start new stack (sim units - meters) 
    private int statusInt;			// used in checking whether balls are still running
    
    /**
     * A simple bNode class for use by bTree.  The "payload" can be
     * modified accordingly to support any object type. <p>
     * 
     * This code has been modified by Jasper Yun to store a reference
     * to an object of type aBall. <p>
     * 
     * @author ferrie
     * 
     */

    class bNode 
    {           		// we need each node to store an aBall object
        aBall ball;     // stores the reference to the aBall
        bNode left;     // reference to left child node
        bNode right;    // reference to right child node
    }

    
    /**
     * addNode method - adds a new node by descending to the leaf node
     *                  using a while loop in place of recursion.  Ugly,
     *                  yet easy to understand.
     */
	
	
    public void addNode(aBall ball) 
    {	
		bNode current;
		
		double bSize = ball.getBSize();		// get the size of the ball being added

        // Empty tree
		if (root == null) root = makeNode(ball);
		
        // If not empty, descend to the leaf node according to
        // the size of the input ball.
        else 
        {
			current = root;
            while (true) 
            {
                if (bSize < current.ball.getBSize()) 		// branch left or right based on ball size
                {
                    // If new ball is smaller than current ball, branch left
                    if (current.left == null) {				// leaf node
						current.left = makeNode(ball);		// attach new node here
						break;
					}
					else current = current.left;			// otherwise, keep traversing
				}
                
                else 
                {
                    // If new ball is larger than current ball, branch right
					
                    if (current.right == null) {  			// leaf node
                        current.right = makeNode(ball);		// attach
                        break;
                    }
                    else current = current.right;			// otherwise keep traversing
				}
			}
		}
		
	}
    
	
    /**
     * makeNode <p>
     * 
     * Creates a single instance of a bNode
     * 
     * @param	aBall ball   Ball to be added
     * @return  bNode node 	 Node created
     */
	
    bNode makeNode(aBall ball) 
    {
		bNode node = new bNode();				// create new node object
		node.ball = ball;						// store reference to the ball
		node.left = null;						// set both successors to null
		node.right = null;
		return node;							// return handle to new object
	}
	
	
    /**
     * stackBalls method - inorder traversal via call to recursive method
     * <code>traverse_Stacking(root)</code> to stack the balls in order of ball size. <p>
     * 
     * @param void
     * @return void
     */
	
    void stackBalls() {
    	traverse_Stacking(root);
    }
    
    /**
     * traverse_inorder method - recursive method to traverse the
     * bTree. Used to stack the balls. <p>
     * 
     * The method is <code>private</code> to hide complexity from
     * the user. <p>
     * 
     * @param bNode root	root node of the bTree
     */
    
    private void traverse_Stacking(bNode root) 
    {
    	if (root.left != null) traverse_Stacking(root.left);	// traverse left until we reach leaf node
   
    	// process for stacking balls:
    	// 		1. Get size of ball at current node.
    	// 		2. Update values of X and Y to determine where to place it.
    	// 		3. moveTo(X,Y) to place the ball there
    	
    	double currentSize = root.ball.getBSize();	// set currentSize to be the size of the current ball
    	
    	if (currentSize - lastSize > DELTASIZE)		// start new stack
    	{
    		X += 2.0 * lastSize;			// move X to right by 2*currentSize
    		Y = currentSize;				// on new stack, the new Y is height of the center of the ball
    		root.ball.moveTo(X, Y);			// move ball
    		
    		
    		lastSize = currentSize;			// reassign the last size to be the size of new ball
    	}
    	else
    	{
    		Y += 2.0 * currentSize;			// set Y to be at new height
    		root.ball.moveTo(X, Y);			// move ball
    		lastSize = currentSize;			// reassign last size
    	}
    	
    	if (root.right != null) traverse_Stacking(root.right);	// traverse right until we reach leaf node
    }
    
    
    
    /**
     * Checks the status of the balls in the bTree by calling 
     * in order traversal method, <code>traverse_status(root, i)</code>. 
     * Returns <li>true if there are any balls still bouncing. 
     * 		   <li>false otherwise. </li> <p>
     * 
     * @return flag - <code>boolean</code> flag indicating whether any balls are still bouncing
     */
    
    boolean isRunning()
    {
    	statusInt = 0;	// each time we call inorder_status(), we start from 0 balls stopped
    	
    	// if the number of balls stopped == number of balls in simulation, then sim is finished;
    	// otherwise, the sim is still running, so return true
    	
    	if (traverse_status(root, statusInt) == bSim.getNUMBALLS()) return false;
    	else return true;
    }
    
    
    /**
     * Checks whether the simulation is still running by
     * checking the bTree recursively. A counter <code>i</code> 
     * is used to track how many balls return false when the
     * aBall <code>isRunning()</code> is interrogated. <p>
     * 
     * @param root - <code>bNode</code> root node of bTree
     * @param i - <code>int</code> Counter tracking number of falses returned by aBall <code>isRunning()</code>
     * @return i - <code>int</code> Counter
     */
    private int traverse_status(bNode root, int i)
    {
    	if (root.left != null)					// traverse left unless root.left == null
    		i = traverse_status(root.left, i);	// recursive step
    	
    	if (root.ball.isRunning());				// check if the root object is running; if true, do nothing
    	else if (!root.ball.isRunning()) i++;	// increment i if isRunning is false
    	
    	if (root.right != null) 				// traverse right unless root.right == null
    		i = traverse_status(root.right, i);	// recursive step
    	
    	return i;	// return number of falses
    }
    
   
}


