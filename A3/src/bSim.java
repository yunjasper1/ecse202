import acm.program.*;
import acm.util.RandomGenerator;
import java.awt.Color;
import acm.graphics.*;
import java.awt.event.*;


/**
 * Jasper Yun - 260651891 <p>
 * ECSE202 Fall 2019 - Assignment 3 <p>
 * 
 * This program contains the code to set up the graphics environment for
 * simulating projectile motion of 60 balls. This program does not take user
 * input. It generates parameters for the aBall constructor in a
 * pseudorandom manner. <p>
 * 
 * Once run, this class will create a display window of 1200 x 800 pixels.
 * The balls will begin on the ground in the middle of the screen. The 
 * simulation will end when the balls no longer have sufficient energy. 
 * The user will then be prompted to click the mouse to stack the balls
 * in order of size.<p>
 * 
 * @author Jasper Yun
 * 
 * @date October 31, 2019
 */


public class bSim extends GraphicsProgram 
{
	
// ---------  INSTANCE VARIABLES: from assignment instructions  --------- //
	
	// display window parameters in pixels
	private static final int WIDTH = 1200; 
	private static final int HEIGHT = 600; 			// distance from top of screen to ground plane
	private static final int OFFSET = 200; 			// distance from ground to bottom of screen
	private static final double SCALE = HEIGHT/100;	// scale factor in pixels/metre
	
	private static final int NUMBALLS = 60;			// number of balls to simulate
	
	// ranges for constructor parameters
	private static final double MINSIZE = 1.0; 		// minimum ball radius in meters
	private static final double MAXSIZE = 7.0;		// maximum ball radius in meters
	
	private static final double EMIN = 0.2;			// minimum energy loss coefficient
	private static final double EMAX = 0.6;			// maximum energy loss coefficient
	
	private static final double V0MIN = 40.0;		// minimum initial velocity (m/s)
	private static final double V0MAX = 50.0;		// maximum initial velocity (m/s)
	
	private static final double ThetaMIN = 80.0;	// minimum launch angle (degrees)
	private static final double ThetaMAX = 100.0;   // maximum launch angle (degrees)
	
	
	// boolean is used to indicate whether the mouse click action is the one we want to stack the balls;
	// because we change the flag CONTINUE to true in the mouseClicked() method, then we
	// will need some way to ensure that CONTINUE is false immediately before we begin waiting
	// for the click to stack the balls; 
	// therefore, we will reassign CONTINUE to false right before the code that waits for the mouse click
	// that will stack the balls; this will ensure that any prior mouse clicks in the program that occur
	// while the balls are bouncing do not affect the code that waits to stack the balls
	private static boolean CONTINUE = false;		
	
	
	
	
	/**
	 * Gets the SCALE defined for bSim class.
	 * 
	 * @param void
	 * @return SCALE
	 */
	
	static double getScale() { return SCALE;}
	
	/**
	 * Gets the NUMBALLS (number of balls) defined
	 * for bSim class.
	 * 
	 * @param void
	 * @return NUMBALLS
	 */
	
	static int getNUMBALLS() { return NUMBALLS; }
	
	/**
	 * Gets the HEIGHT defined for bSim class.
	 * 
	 * @param void
	 * @return HEIGHT
	 */
	static int getHEIGHT() { return HEIGHT; }
	
	/**
	 * adds mouseListeners to the bSim program.
	 */
    public void init() 
    { 
	    addMouseListeners(); 
    }
   
    
    /**
     * Sets <code>CONTINUE</code> to true when the mouse is clicked. <p>
     * 
     * @param e - <code>MouseEvent</code>
     */
    public void mouseClicked(MouseEvent e) { CONTINUE = true; }
    
     
    /**
	 * Creates the graphics environment and resizes the display canvas window.
	 * The method also uses RandomGenerator rgen to pseudorandomly generate 
	 * values for the parameters of the aBall constructor. These values
	 * are passed into the aBall constructor 60 times to create 60 balls
	 * for the simulation. <p>
	 * 
	 * Stacks the balls upon user click once the balls stop bouncing. <p>
	 * 
	 * @param void
	 * @return void
	 */
	
	public void run() 
	{
		this.resize(WIDTH, HEIGHT + OFFSET); 		// resizing the window
		init();										// adds mouse listeners
		
		//println(CONTINUE);
	
		
	// -----  GRAPHICS: ground and display window resizing  ----- //
		
		// draw graphics display and ball:
		GRect ground = new GRect(0,HEIGHT,WIDTH,3);
		ground.setFilled(true);
		ground.setFillColor(Color.BLACK);
		
		// add ground and ball to canvas
		add(ground);
		
		RandomGenerator rgen = RandomGenerator.getInstance();	// create instance of random generator variable
		rgen.setSeed((long) 424242);							// set seed of random generator
		
		// create bTree to store balls
		bTree myTree = new bTree();
		
		
		// declare variables for the constructor
		double Xi, Yi, V0, theta, bSize, bLoss;
		Color bColor;
		
		
		// generating parameters for aBall constructor -- this is my code
		
		for (int i = 0; i < NUMBALLS; i++)					// for loop runs 60 times to create 60 different balls
		{
			bSize = rgen.nextDouble(MINSIZE, MAXSIZE);		// generate random values for aBall constructor
			bColor = rgen.nextColor();						// generate in the order specified by Prof. Ferrie
			bLoss = rgen.nextDouble(EMIN, EMAX);			
			V0 = rgen.nextDouble(V0MIN, V0MAX);				
			theta = rgen.nextDouble(ThetaMIN, ThetaMAX);	
			
			Xi = (WIDTH / 2) / SCALE; 	    // all balls begin their adventure at the middle of the screen
			Yi = bSize; 					// starting position of balls on screen
			
			// creating ball
			aBall newBall = new aBall(Xi, Yi, V0, theta, bSize, bColor, bLoss);
			add(newBall.getBall());	// add ball to screen
			myTree.addNode(newBall);// add ball to bTree
			newBall.start(); 		// start the thread of the ball created
			 	
		}
		
		
		// block any action until all balls terminate
		while (myTree.isRunning());	
		
		
		// Code to add a GLabel to the display and prompt user
		// the location of the label (1015, 585) was found through trial and error
		GLabel userPrompt = new GLabel("CR to continue", 1015, 585);
		userPrompt.setColor(Color.RED);
		add(userPrompt);
		
		CONTINUE = false;	// ensures that prior mouse clicks do not change CONTINUE to true
		
		while (!myTree.isRunning()) // for some reason, while (true) didn't work
		{

			//System.out.println("Inside while:" + CONTINUE);
			
			if (CONTINUE)			// mouse click will set CONTINUE to true and run the code
			{						// which stacks the balls;
				
				remove(userPrompt); // remove previous GLabel
				myTree.stackBalls();// stack balls
				break;				// exit while loop
			}
		}

		// show final message
		// the location of the label (1015, 585) was found through trial and error
		GLabel finished = new GLabel("All stacked!", 1015, 585);
		finished.setColor(Color.RED);
		add(finished);
		
	}
		
}

