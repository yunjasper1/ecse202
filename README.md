# ECSE202
Files for ECSE202 Fall 2019.
This repo contains files for all of the assignments.

## A1 - Ball Drop

This assignment is based on Chapter 2 of the textbook by Eric Roberts, "Programming by Example". It simulates the trajectory of a ball in projectile motion according to Newtonian mechanics. <br>

The program takes user inputs of ball size, initial velocity, launch angle, and energy loss percentage. It displays the trajectory of the ball with tracing points.

## A2 - Multiple Ball Drop

This assignment builds on Assignment 1. It simulates the bouncing of 100 balls concurrently, by creating a file <code>bSim.java</code> which extends Thread. The balls are of different sizes, initial velocities, launch angles, energy loss percentages, and colors. These parameters are randomly generated using <code>RandomGenerator</code> and passed into the constructor for the <code>aBall</code> class. The balls are launched from the center of the screen, on the ground.

## A3 - Stacking Balls Using Binary Tree

This assignment builds on Assignment 2. After the balls have stopped bouncing, they are stacked once the user clicks the mouse button. The references to <code>aBall</code> objects are stored in a bTree. While the balls are still bouncing, the user cannot stack the balls. After the balls have stopped, a prompt is displayed on the screen. Once the mouse is clicked, the balls are stacked according to size.

## A4 - GUI in Java

This assignment builds on assignment 3. A GUI is implemented to allow users to control the ranges for the random generation of input parameters. Buttons for executing commands are added also (stacking balls, stopping simulations, adding trace points, clearing the screen, and running simulations). Users are able to input values for parameter ranges using input boxes or by dragging sliders.

## A5 - Ball Drop in C

This assignment takes the code from assignment 1 and changes it to C. The simulation is run and the positions of the ball are printed on the command line.

## A6 - Student Database in C

This assignment implements binary tree traversal and searching in C. Student names, IDs, and marks are read in from two text files. The program provides a command line interface to search for student records. <br> 

The majority of the code was written by Prof. Ferrie; I wrote the code for adding nodes to the bTree, traversing, and searching the bTree for matches by either student ID or by student last name.
