import acm.program.*;
import acm.util.RandomGenerator;
import java.awt.*;

import acm.graphics.*;
import acm.gui.*;

import java.awt.event.*;
import javax.swing.*;


/**
 * Jasper Yun - 260651891 <p>
 * ECSE202 Fall 2019 - Assignment 4 <p>
 * 
 * This program contains the code to set up the graphics environment for
 * simulating projectile motion of balls. This program is interactive. 
 * The user sets ranges for parameters, and the program generates 
 * parameters for the aBall constructor in a pseudorandom manner. <p>
 * 
 * Once run, this class will create a display window of 1530 x 800 pixels.
 * The balls will begin on the ground in the middle of the screen. The 
 * simulation will end when the balls no longer have sufficient energy. 
 * The user will then be prompted to take other action by using buttons. <p>
 * 
 * @author Jasper Yun
 * 
 * @date November 11, 2019
 */


public class bSim extends GraphicsProgram 
{
	
// ---------  PARAMETERS: from assignment instructions  --------- //
	
	// display window parameters in pixels
	private static final int WIDTH = 1200;
	private static final int HEIGHT = 600; 			// distance from top of screen to ground plane
	private static final int OFFSET = 200; 			// distance from ground to bottom of screen
	private static final double SCALE = HEIGHT/100;	// scale factor in pixels/metre
	
	private static int NUMBALLS = 60;			// number of balls to simulate
	
	// default values for constructor parameters
	private static double MINSIZE = 1.0; 		// minimum ball radius in meters
	private static double MAXSIZE = 7.0;		// maximum ball radius in meters
	
	private static double EMIN = 0.2;			// minimum energy loss coefficient
	private static double EMAX = 0.6;			// maximum energy loss coefficient
	
	private static double V0MIN = 40.0;			// minimum initial velocity (m/s)
	private static double V0MAX = 50.0;			// maximum initial velocity (m/s)
	
	private static double ThetaMIN = 80.0;		// minimum launch angle (degrees)
	private static double ThetaMAX = 100.0;  	// maximum launch angle (degrees)
	
	private static RandomGenerator rgen;
	
	// other variables required to make my program run without errors
	private static boolean PRINTTRACES = false;	// indicates whether to print traces
	private static boolean simEnable;			// indicates whether to run simulation
	private static int PANELWIDTH;				// JPanel width
	
	private static GLabel userPrompt;			// textual user prompt "Click to continue"
	private static GLabel simulating;			// textual indication that simulation is currently running
	private static GLabel stacked;				// textual indication that balls have been stacked
	private static boolean doneStack;			// boolean flag indicating balls were stacked
	private static boolean cleared = true;		// boolean flag indicating the "Clear Screen" button was pressed
	
	
	private bTree myTree;						// bTree
	
	// needed for the GUI
	private static double PRECISION = 100;		// indicate precision of the doubles in the fields (100 = two decimal places);
												// precision needs to be a double to perform division properly 
	
	// instance variables for each Int/DoubleField, and each slider
	// we need instance variables for each so that we can update values
	// for both the Int/DoubleFields and sliders when one of them is updated
	
	private static JSlider numbBallsSlider;
	private static IntField numbBallsValue;

	private static JSlider minSizeSlider;
	private static DoubleField minSizeValue;

	private static JSlider maxSizeSlider;
	private static DoubleField maxSizeValue;

	private static JSlider minLossSlider;
	private static DoubleField minLossValue;

	private static JSlider maxLossSlider;
	private static DoubleField maxLossValue;

	private static JSlider minVelSlider;
	private static DoubleField minVelValue;

	private static JSlider maxVelSlider;
	private static DoubleField maxVelValue;

	private static JSlider minThetaSlider;
	private static DoubleField minThetaValue;

	private static JSlider maxThetaSlider;
	private static DoubleField maxThetaValue;
	
	
	// instance variables for buttons that trigger actions
	private static JButton simulate;		// button to run simulation
	private static JButton traces;			// button to toggle traces
	private static JLabel tracesState;		// label indicating the current toggle state for traces
	private static JButton exit;			// button to exit program
	
	
	
	// ---------------------------  GETTERS AND SETTERS  --------------------------- //
	
	/**
	 * Gets the instance variable <code>OFFSET</code>. <p>
	 * 
	 * @param void
	 * @return OFFSET - <code>int</code> <code>OFFSET</code> instance variable in bSim class
	 */
	static int getOFFSET() { return bSim.OFFSET; }
	
	
	/**
	 * Gets the SCALE defined for bSim class.
	 * 
	 * @param void
	 * @return SCALE - <code>int</code> <code>SCALE</code> instance variable in bSim class
	 */
	
	static double getScale() { return SCALE;}
	
	/**
	 * Gets the <code>NUMBALLS</code> (number of balls) defined
	 * for bSim class. <p>
	 * 
	 * @param void
	 * @return NUMBALLS - <code>int</code> <code>NUMBALLS</code> instance variable in bSim class
	 */
	
	static int getNUMBALLS() { return NUMBALLS; }
	
	/**
	 * Gets the <code>HEIGHT</code> defined for bSim class. <p>
	 * 
	 * @param void
	 * @return HEIGHT - <code>int</code> <code>HEIGHT</code> instance variable in bSim class
	 */
	static int getHEIGHT() { return HEIGHT; }


	/**
     * Gets the boolean flag <code>PRINTTRACES</code>.<p>
     * 
     * 
     * @return PRINTTRACES - <code>boolean</code> Flag representing whether to print traces
     * 
     */
    boolean getPRINTTRACES() {return PRINTTRACES;}
	
	
 
    
    /**
     * resets the canvas by first removing all objects, then drawing 
     * the ground plane back in. <p>
     * 
     * @param void
     * @return void
     */
    
    private void resetDisplay()
    {
    	// remove all objects from screen
    	removeAll();
    	
    	drawGround();	// redraw the ground
    }
    
    
    /**
     * Draws the ground plane by adding a rectangle to 
     * the canvas.
     * 
     * @param void
     * @return void
     */
    private void drawGround() {
    	
    	// add ground back to the canvas
    	GRect ground = new GRect(0,HEIGHT,WIDTH,3);
		ground.setFilled(true);
		ground.setFillColor(Color.BLACK);
		add(ground);
    }
    
    // -------------  BUTTON AND INT/DOUBLEFIELD LISTENERS  ------------------ //
    
    /**
     * This method is called when any of the buttons are clicked on 
     * or when values are entered into the Int/DoubleFields. <p>
     * 
     * @param void
     * @return void
     */
    public void actionPerformed(ActionEvent e) 
    { 
    	// when the "Traces" button is pressed
        if (e.getActionCommand().equals("Traces")) {
        	
        	PRINTTRACES = !PRINTTRACES;			// toggle PRINTTRACES boolean, which makes aBall print traces
        	if (PRINTTRACES == true)
        		tracesState.setText("On");		// change label to indicate that PRINTTRACES = true
        	else
        		tracesState.setText("Off");		// change label to indicate that PRINTTRACES = false
        	
        }
        
        
        // when the "Clear Screen" button is pressed
        else if (e.getActionCommand().equals("Clear Screen")) {
        	
        	resetDisplay(); 					// removes all objects from canvas, draws ground
        	//myTree.printTree();				// for testing, print out the bTree nodes
        	doneStack = false;
        	//if (doneStack) remove(stacked);	// remove prompt indicating balls were stacked
        	myTree.clearAll();					// "kills" each ball
        	//cleared = true;					// flag indicating the button was pressed
        	
        }
        
        
        // when the "Stop" button is pressed
        else if (e.getActionCommand().equals("Stop")) {
        	
        	if (myTree.getRoot() != null) {			// the if statement prevents errors if the bTree is empty
	        	myTree.pause();						// pauses each ball by setting RUNNING = false
	        	
	        	
	        	// this block of code removes any text (GLabel object) at the point (1015, 625)
	        	// when simulations are run
	        	if (getElementAt(1015,625) != null) {
	        		if (getElementAt(1015,625) instanceof GLabel)
	        			remove(getElementAt(1015,625));
	        	}
	        	
	        	
	        	// label to indicate that the simulation is terminated
	        	GLabel stopped = new GLabel("Simulation terminated", 1015, 625);
	        	stopped.setColor(Color.RED);
	        	
	        	add(stopped);						// add indicator
	        	pause(5);							// pause to ensure stopped is added before userPrompt is removed
	        	//remove(userPrompt);				// remove userPrompt which appears when balls stop bouncing
	        	
	        }
        }
        
        
        // when the "Stack Balls" button is pressed
        else if (e.getActionCommand().equals("Stack Balls")) {
        	
        	if (!myTree.isRunning())		// stack balls will execute only if the balls are not bouncing
        	{
        		//resetDisplay();			// gets rid of the traces
	        	//myTree.addBallBack(this);	// add balls back to screen; this was used when my program cleared the screen
        									// to remove traces when "Stack Balls" was clicked.
        		
	        	myTree.stackBalls();		// stack balls
	        	
	        	// this block of code removes any text (GLabel object) at the point (1015, 625)
	        	// when simulations are run; I should have made it a method...
	        	while (true) {
	        		if (getElementAt(1015,625) != null) {
		        		if (getElementAt(1015,625) instanceof GLabel)
		        			remove(getElementAt(1015,625));
	        			}
	        		else break;
	        	}
	        	
	        	// indicate to user that balls have finished stacking
	        	//if (!cleared) {		// if cleared is not true, then the screen was not cleared
	        		stacked = new GLabel("All stacked!", 1015, 625);
	        		stacked.setColor(Color.RED);
	        		add(stacked);
	        		cleared = false;
	        	}
	        	
        	// I was unable to get this part to work properly when different button orders were tested
        	/*
	        	else if (cleared) { // this is if the screen was cleared
	        	
		        	stacked = new GLabel("No balls to stack", 1015, 625);
		        	stacked.setColor(Color.RED);
		        	add(stacked);
		        	//println("Stacked");
		        	doneStack = true;			// indicate that balls were stacked
		        	cleared = false;
	        	}
	        	
	        */
        	//}
        }
        
        // when the "Simulate" button is pressed
        else if (e.getActionCommand().equals("Simulate!")) {
        	
        	//println("doSim");	// for testing
        	
        	if (doneStack) {		// if balls have been stacked, reset the display on new simulation run
        		//resetDisplay();		// resets display
        		//doneStack = false;	// reset doneStack to false
        	}
        	
        	// this block of code removes any text (GLabel object) at the point (1015, 625)
        	// when simulations are run
        	if (getElementAt(1015,625) != null) {
        		if (getElementAt(1015,625) instanceof GLabel)
        			remove(getElementAt(1015,625));
        	}
        	
        	// add textual indicator that simulation is running
        	simulating = new GLabel("Running Simulation...", 1015, 625);
        	simulating.setColor(Color.RED);
        	add(simulating);
        	
        	simEnable = true;	// run doSim in the while loop in the run() method
        }
        
        // when the "Exit Program" button is pressed
        else if (e.getActionCommand().equals("Exit Program")) {
        	
        	//println("Exiting");	// for testing
        	//resetDisplay();
        	System.exit(0);		// exit program
        }
        
        /* 
         * the following code is for updating the parameters based on user inputs:
         * when values are entered into the Int/DoubleFields, we update the 
         * corresponding instance variables:
         *		- NUMBALLS
         *		- MINSIZE
         *		- MAXSIZE
         *		- EMIN
         *		- EMAX
         *		- V0MIN
         *		- V0MAX
         *		- ThetaMAX
         *		- ThetaMIN
         * 
         * we also change the value displayed on the sliders for each of the above
         * instance variables that were changed by using sliderName.setValue(value)
         *
         * note that sliders can display integers only, so we use an integer that is
         * 100 times larger, then divide that value by 100 when we retrieve its value
         * we use 100 to be able to display decimal values to 2 decimal places; however,
         * we can obtain more precision by setting the instance variable PRECISION to
         * a different power of 10
         *
         * note that we do not need to prevent user inputs that make the maximum for
         * a parameter less than the minimum for that parameter, since RandomGenerator 
         * takes care of that, i.e. doing nextDouble(5,0) and nextDouble(0,5) will produce
         * the same results.
         * 
         * note also that we use else if instead of if because we can input only to one 
         * Int/DoubleField at a time (as human users).
         * 
         * idea to work around only using integers with sliders: 
    	 * https://stackoverflow.com/questions/2172574/jslider-for-doubles
    	 * 
         * 
         */
        
        // number of balls
        else if (e.getSource() == numbBallsValue) {
        	
        	//println("Hello, " + numbBallsValue.getValue());	// for testing
        	NUMBALLS = numbBallsValue.getValue();				// update instance variable with IntField value
        	numbBallsSlider.setValue(NUMBALLS);					// set slider value to updated instance variable value
        }
        
        // minimum ball size
        else if (e.getSource() == minSizeValue) {
        	
        	//println("Hello, " + minSizeValue.getValue());	// for testing
        	MINSIZE = minSizeValue.getValue();				// update instance variable with DoubleField value
        	minSizeSlider.setValue( (int) (MINSIZE * PRECISION));	// update value of slider
        	
        }
        
        // maximum ball size
        else if (e.getSource() == maxSizeValue) {
        	
        	//println("Hello, " + maxSizeValue.getValue());
        	MAXSIZE = maxSizeValue.getValue();
        	maxSizeSlider.setValue( (int) (MAXSIZE * PRECISION));
        }
        
        // minimum energy loss
        else if (e.getSource() == minLossValue) { 
        	
            //println("Hello, " + minLossValue.getValue()); 
            EMIN = minLossValue.getValue();
            minLossSlider.setValue( (int) (EMIN * PRECISION));
         } 
        
        // maximum energy loss
        else if (e.getSource() == maxLossValue) { 
        	
            //println("Hello, " + maxLossValue.getValue()); 
            EMAX = maxLossValue.getValue();
            maxLossSlider.setValue( (int) (EMAX * PRECISION));
         } 
        
        
        // minimum velocity
        else if (e.getSource() == minVelValue) { 
        	
            //println("Hello, " + minVelValue.getValue()); 
            V0MIN = minVelValue.getValue();
            minVelSlider.setValue( (int) (V0MIN * PRECISION));
         } 
        
        // maximum velocity
        else if (e.getSource() == maxVelValue) { 
        	
            //println("Hello, " + maxVelValue.getValue());
            V0MAX = maxVelValue.getValue();
            maxVelSlider.setValue( (int) (V0MAX * PRECISION));
         } 
        
        // minimum launch angle
        else if (e.getSource() == minThetaValue) { 
        	
            //println("Hello, " + minThetaValue.getValue()); 
            ThetaMIN = minThetaValue.getValue();
            minThetaSlider.setValue( (int) (ThetaMIN * PRECISION));
         } 
        
        // maximum launch angle
        else if (e.getSource() == maxThetaValue) { 
        	
            //println("Hello, " + maxThetaValue.getValue());
            ThetaMAX = maxThetaValue.getValue();
            maxThetaSlider.setValue( (int) (ThetaMAX * PRECISION));
         } 
     } 
    
   
    
    // -------------------------  SLIDER LISTENERS  ----------------------------- //
    
    /**
	 * Called when the mouse is released. This method is used
	 * to update the instance variables when the sliders have been
	 * adjusted or clicked on. <p>
	 * 
	 * @param void
	 * @return void
	 */
    public void mouseReleased(MouseEvent e) {
    	
    	/*
    	 * much like the actionPerformed() method above, this method will update
    	 * the appropriate instance variable depending on which slider is adjusted.
    	 * 
    	 * It will also update the Int/DoubleFields beside the slider to show the 
    	 * user the value displayed on the slider. Because sliders can display integers
    	 * only, we divide the slider value by PRECISION (in this case, 100) and display
    	 * the result in the Int/DoubleField.
    	 * 
    	 * Note that PRECISION is a double, so that we can get decimals instead of integers.
    	 * 
    	 * Note that we use else if instead of if because we can change one slider at a time
    	 * only (as human users).
    	 * 
    	 * idea to work around only using integers with sliders: 
    	 * https://stackoverflow.com/questions/2172574/jslider-for-doubles
    	 * 
    	 */
    	
    	// number of balls
    	if (e.getSource() == numbBallsSlider)
    	{
    		NUMBALLS = numbBallsSlider.getValue();	// update instance variable with the value of the slider
    		numbBallsValue.setValue(NUMBALLS);		// update IntField to display new value of instance variable
    		//println(NUMBALLS);					// for testing
    	}
    	
    	// minimum ball size
    	else if (e.getSource() == minSizeSlider)
    	{
    		MINSIZE = minSizeSlider.getValue() / PRECISION;	// update instance variable with value of slider / 100
    		minSizeValue.setValue(MINSIZE);					// update DoubleField to display new value
    		//println(MINSIZE);								// for testing
    	}
    	
    	// maximum ball size
    	else if (e.getSource() == maxSizeSlider)
    	{
    		MAXSIZE = maxSizeSlider.getValue() / PRECISION;
    		maxSizeValue.setValue(MAXSIZE);
    		//println(MAXSIZE);
    	}
    	
    	// minimum energy loss
    	else if (e.getSource() == minLossSlider)
    	{
    		EMIN = minLossSlider.getValue() / PRECISION;
    		minLossValue.setValue(EMIN);
    		//println(EMIN);
    	}
    	
    	// maximum energy loss
    	else if (e.getSource() == maxLossSlider)
    	{
    		EMAX = maxLossSlider.getValue() / PRECISION;
    		maxLossValue.setValue(EMAX);
    		//println(EMAX);
    	}
    	
    	// minimum velocity
    	else if (e.getSource() == minVelSlider)
    	{
    		V0MIN = minVelSlider.getValue() / PRECISION;
    		minVelValue.setValue(V0MIN);
    		//println(V0MIN);
    	}
    	
    	// maximum velocity
    	else if (e.getSource() == maxVelSlider)
    	{
    		V0MAX = maxVelSlider.getValue() / PRECISION;
    		maxVelValue.setValue(V0MAX);
    		//println(V0MAX);
    	}
    	
    	// minimum launch angle
    	else if (e.getSource() == minThetaSlider)
    	{
    		ThetaMIN = minThetaSlider.getValue() / PRECISION;
    		minThetaValue.setValue(ThetaMIN);
    		//println(ThetaMIN);
    	}
    	
    	// maximum launch angle
    	else if (e.getSource() == maxThetaSlider)
    	{
    		ThetaMAX = maxThetaSlider.getValue() / PRECISION;
    		maxThetaValue.setValue(ThetaMAX);
    		//println(ThetaMAX);
    	}
    	
    }
    
    
   
    /**
	 * The method uses  <code>RandomGenerator rgen</code> to pseudorandomly 
	 * generate values for the parameters of the aBall constructor. The 
	 * random generator seed is set in the <code>init()</code> method to 
	 * prevent the seed from being reset each time <code>doSim()</code> is run. <p>
	 * 
	 * The random generator values are passed into the aBall constructor to 
	 * create a certain number of balls for the simulation. The number of balls 
	 * depends on user input. The default is 60 balls. <p>
	 * 
	 * The method terminates when the balls stop bouncing. <p>
	 * 
	 * @param void
	 * @return void
	 */
	
	void doSim()
	{
		
		// pre-processing variables:
		
		//println("got inside doSim");
		
		
		// declare variables for the constructor
		double Xi, Yi, V0, theta, bSize, bLoss;
		Color bColor;
		
		
		// generating parameters for aBall constructor -- this is my code
		
		for (int i = 0; i < NUMBALLS; i++)					// for loop runs 60 times to create 60 different balls
		{
			// note that in the random generation of values,
			// we do not need to ensure that what we call MAX, MIN do not need to satisfy MAX > MIN
			// ACM has taken care of that in the random generation
			// i.e. errors will not arise if we do e.g. rgen.nextDouble(15, 2) instead of rgen.nextDouble(2,15)
			
			bSize = rgen.nextDouble(MINSIZE, MAXSIZE);		// generate random values for aBall constructor
			bColor = rgen.nextColor();						// generate in the order specified by Prof. Ferrie
			bLoss = rgen.nextDouble(EMIN, EMAX);			
			V0 = rgen.nextDouble(V0MIN, V0MAX);				
			theta = rgen.nextDouble(ThetaMIN, ThetaMAX);	
			
			Xi = (WIDTH / 2) / SCALE; 	    // all balls begin their adventure at the middle of the screen
			Yi = bSize; 					// starting position of balls on screen
			
			// creating ball
			aBall newBall = new aBall(Xi, Yi, V0, theta, bSize, bColor, bLoss, this);
			add(newBall.getBall());	// add ball to screen
			myTree.addNode(newBall);// add ball to bTree
			newBall.start(); 		// start the thread of the ball created
			 	
		}
		
		// after balls stop bouncing:
		while (true) {
			if (myTree.isRunning() == false) {
				
				// remove any previous GLabel on the screen
				if (getElementAt(1015,625) != null) {
	        		if (getElementAt(1015,625) instanceof GLabel)
	        			remove(getElementAt(1015,625));
	        	}
				
				// Code to add a GLabel to the display and prompt user
				userPrompt = new GLabel("Simulation terminated", 1015, 625);
				userPrompt.setColor(Color.RED);
				add(userPrompt);
				break;
			}
		}
				
	}
	
	
	/**
	 * Run method of the program. It implements a 
	 * <code>while(true)</code> loop that polls the state 
	 * of the boolean variable <code>simEnable</code>. <p>
	 * 
	 * If <code>simEnable</code> is true, then the while 
	 * loop will run the <code>doSim()</code> method. Otherwise,
	 * the run method waits for <code>simEnable</code> to be true again. <>
	 * 
	 * This code was written by Prof. Ferrie.
	 * 
	 * @param void
	 * @return void
	 */
	public void run()
	{
		simEnable = false;
		
	    // this code was given by Prof. Ferrie
	    while(true) {
	    	
			pause(200);
			if (simEnable) { 		// Run once, then stop
				doSim();
				simEnable=false;
			}
		}
	}
        
	
	/**
	 * This method performs the following:
	 * <li> resizes the window to fit the simulation window and the panel for user input
	 * <li> creates sliders for user input
	 * <li> creates Int/DoubleFields for user input
	 * <li> adds labels to each slider
	 * <li> adds buttons to run or stop the program
	 * <li> adds dropdown menus at the top of the window (that don't do anything)
	 * </li> <p>
	 * 
	 * The <code>init()</code> method also contains a <code>while(true)</code> loop that ensures that 
	 * the display is updated appropriately as the program runs. This workaround
	 * was given by Prof. Ferrie since we wrote this program by extending GraphicsProgram. <p>
	 * 
	 * @param void
	 * @return void
	 */
	
	@SuppressWarnings("unchecked")
	public void init()
	{

		simEnable = false; 		// this is an instance variable
	
		JPanel myPanel = new JPanel();
    	
    	//myPanel.setSize(330, HEIGHT + OFFSET);	
    	myPanel.setLayout(new TableLayout(30, 5));
    	
    	/* 
    	 * some integers for the constraints on each component
    	 * these are placed here so that the appearance of the
    	 * GUI can be easily modified
    	 */
    	int gridHeight = 20;
    	int labelWidth = 90;
    	int sliderWidth = 100;
    	int minLabelWidth = 20;
    	int maxLabelWidth = 60;
    	int fieldWidth = 50;
    	
    	PANELWIDTH = labelWidth + sliderWidth + minLabelWidth + maxLabelWidth + fieldWidth;
    	
    	/*
    	 * constraints for the panel
    	 * again, we place these here so that the GUI appearance
    	 * is easily modified
    	 */
    	String emptyLine = "gridWidth=5 height=10";
    	
    	String labelConstraint = "width=" + labelWidth + " height=" + gridHeight;
    	String minLabelConstraint = "width=" + minLabelWidth + " height=" + gridHeight;
    	String maxLabelConstraint = "width=" + maxLabelWidth + " height=" + gridHeight;
    	String sliderConstraint = "width=" + sliderWidth + " height=" + gridHeight;
    	String fieldConstraint = "width=" + fieldWidth + " height=" + gridHeight;
	    
    	
    	// title for the JPanel
    	myPanel.add(new JLabel("General Simulation Parameters"), "height=50 gridWidth=5 anchor=CENTER");
	    
    	
    	
    	/*
    	 * For greater ease of editing, it would have been smarter to make 
    	 * instance variables for the absolute maximums and minimums for each
    	 * of the sliders and Int/DoubleFields. However, I didn't do that...sadly
    	 */
	    
	    
	    // ----------------- NUMBER OF BALLS ------------------ //
	    
	    myPanel.add(new JLabel("NUMBALLS:"),  labelConstraint);
	    myPanel.add(new JLabel("1"), minLabelConstraint);
	    
	    numbBallsSlider = new JSlider(1, 255, NUMBALLS);			// number of balls slider
	    numbBallsSlider.addMouseListener(this);
	    myPanel.add(numbBallsSlider,  sliderConstraint);
	    
	    myPanel.add(new JLabel("255"), maxLabelConstraint);
	    
	    numbBallsValue = new IntField(NUMBALLS, 1, 255);			// number of balls IntField
	    numbBallsValue.addActionListener(this);
	    myPanel.add(numbBallsValue, fieldConstraint);
	    
	    myPanel.add(new JLabel(""), emptyLine);
	    
	    
	    
	    // -----------------  MINIMUM BALL SIZE --------------- //
	    
	    myPanel.add(new JLabel("MIN SIZE:"),  labelConstraint);
	    myPanel.add(new JLabel("1.0"), minLabelConstraint);
	    
	    minSizeSlider = new JSlider(100, 2500, (int) (MINSIZE * PRECISION));	// minimum ball size slider
	    minSizeSlider.addMouseListener(this);
	    myPanel.add(minSizeSlider,  sliderConstraint);
	    
	    myPanel.add(new JLabel("25.0"), maxLabelConstraint);
	    
	    minSizeValue = new DoubleField(MINSIZE, 1.0, 25.0);						// minimum ball size DoubleField
	    minSizeValue.addActionListener(this);
	    myPanel.add(minSizeValue, fieldConstraint);
	    myPanel.add(new JLabel(""), emptyLine);
	    
	    
	    
	    // ----------------- MAXIMUM BALL SIZE ---------------- //
	    
	    myPanel.add(new JLabel("MAX SIZE:"),  labelConstraint);
	    myPanel.add(new JLabel("1.0"), minLabelConstraint);
	    
	    maxSizeSlider = new JSlider(100, 2500, (int) (MAXSIZE * PRECISION));	// maximum ball size slider
	    maxSizeSlider.addMouseListener(this);
	    myPanel.add(maxSizeSlider,  sliderConstraint);
	    
	    myPanel.add(new JLabel("25.0"), maxLabelConstraint);
	    
	    maxSizeValue = new DoubleField(MAXSIZE, 1.0, 25.0);						// maximum ball size DoubleField
	    maxSizeValue.addActionListener(this);
	    myPanel.add(maxSizeValue, fieldConstraint);
	    
	    myPanel.add(new JLabel(""), emptyLine);
	    
	    
	    
	    // ----------------- MINIMUM ENERGY LOSS -------------- //
	    
	    myPanel.add(new JLabel("MIN LOSS:"),  labelConstraint);
	    myPanel.add(new JLabel("0.0"), minLabelConstraint);
	    
	    minLossSlider = new JSlider(0, 100, (int) (EMIN * PRECISION));			// minimum energy loss slider
	    minLossSlider.addMouseListener(this);
	    myPanel.add(minLossSlider,  sliderConstraint);
	    
	    myPanel.add(new JLabel("1.0"), maxLabelConstraint);
	    
	    minLossValue = new DoubleField(EMIN, 0.0, 1.0);							// minimum energy loss DoubleField
	    minLossValue.addActionListener(this);
	    myPanel.add(minLossValue, fieldConstraint);
	    
	    myPanel.add(new JLabel(""), emptyLine);
	    
	    
	    
	    // ----------------- MAXIMUM ENERGY LOSS -------------- //
	    
	    myPanel.add(new JLabel("MAX LOSS:"),  labelConstraint);
	    myPanel.add(new JLabel("0.0"), minLabelConstraint);
	    
	    
	    maxLossSlider = new JSlider(0, 100, (int) (EMAX * PRECISION));			// maximum energy loss slider
	    maxLossSlider.addMouseListener(this);
	    myPanel.add(maxLossSlider,  sliderConstraint);
	    
	    myPanel.add(new JLabel("1.0"), maxLabelConstraint);
	    
	    maxLossValue = new DoubleField(EMAX, 0.0, 1.0);							// maximum energy loss DoubleField
	    maxLossValue.addActionListener(this);
	    myPanel.add(maxLossValue, fieldConstraint);
	    
	    myPanel.add(new JLabel(""), emptyLine);
	    
	    
	    
	    // ----------------- MINIMUM VELOCITY ----------------- //
	    
	    myPanel.add(new JLabel("MIN VEL:"),  labelConstraint);
	    myPanel.add(new JLabel("1.0"), minLabelConstraint);
	    
	    minVelSlider = new JSlider(100, 20000, (int) (V0MIN * PRECISION));		// minimum velocity slider
	    minVelSlider.addMouseListener(this);
	    myPanel.add(minVelSlider,  sliderConstraint);
	    
	    myPanel.add(new JLabel("200.0"), maxLabelConstraint);
	    
	    minVelValue = new DoubleField(V0MIN, 1.0, 200.0);						// minimum velocity DoubleField
	    minVelValue.addActionListener(this);
	    myPanel.add(minVelValue, fieldConstraint);
	    
	    myPanel.add(new JLabel(""), emptyLine);
	    
	    
	    
	    // ----------------- MAXIMUM VELOCITY ----------------- //
	    
	    myPanel.add(new JLabel("MAX VEL:"),  labelConstraint);
	    myPanel.add(new JLabel("1.0"), minLabelConstraint);
	    
	    maxVelSlider = new JSlider(100, 20000, (int) (V0MAX * PRECISION));		// maximum velocity slider
	    maxVelSlider.addMouseListener(this);
	    myPanel.add(maxVelSlider,  sliderConstraint);
	    
	    myPanel.add(new JLabel("200.0"), maxLabelConstraint);
	    
	    maxVelValue = new DoubleField(V0MAX, 1.0, 200.0);						// maximum velocity DoubleField
	    maxVelValue.addActionListener(this);
	    myPanel.add(maxVelValue, fieldConstraint);
	    
	    myPanel.add(new JLabel(""), emptyLine);
	    
	    
	    
	    // ----------------- MINIMUM LAUNCH ANGLE ------------- //
	    
	    myPanel.add(new JLabel("MIN THETA:"),  labelConstraint);
	    myPanel.add(new JLabel("1.0"), minLabelConstraint);
	    
	    minThetaSlider = new JSlider(100, 18000, (int) (ThetaMIN * PRECISION));	// minimum launch angle slider
	    minThetaSlider.addMouseListener(this);
	    myPanel.add(minThetaSlider,  sliderConstraint);
	    
	    myPanel.add(new JLabel("180.0"), maxLabelConstraint);
	    
	    minThetaValue = new DoubleField(ThetaMIN, 1.0, 180.0);					// minimum launch angle DoubleField
	    minThetaValue.addActionListener(this);
	    myPanel.add(minThetaValue, fieldConstraint);
	    
	    myPanel.add(new JLabel(""), emptyLine);
	    
	    
	    
	    // ----------------- MAXIMUM LAUNCH ANGLE ------------- //
	    
	    myPanel.add(new JLabel("MAX THETA:"),  labelConstraint);
	    myPanel.add(new JLabel("1.0"), minLabelConstraint);
	    
	    maxThetaSlider = new JSlider(100, 18000, (int) (ThetaMAX * PRECISION));	// maximum launch angle slider
	    maxThetaSlider.addMouseListener(this);
	    myPanel.add(maxThetaSlider,  sliderConstraint);
	    myPanel.add(new JLabel("180.0"), maxLabelConstraint);
	    
	    maxThetaValue = new DoubleField(ThetaMAX, 1.0, 180.0);					// maximum launch angle DoubleField
	    maxThetaValue.addActionListener(this);
	    myPanel.add(maxThetaValue, fieldConstraint);
	    
	    myPanel.add(new JLabel(""), "gridWidth=5 height=25"); // empty line
	    
	    
	    
	    // BUTTONS FOR PROGRAM CONTROL
	    
    	simulate = new JButton("Simulate!");
    	simulate.addActionListener(this);
    	myPanel.add(simulate, "gridWidth=5 height=30");
    	
    	myPanel.add(new JLabel(""), "gridWidth=5 height=10"); // empty line
    	
    	// another title
    	myPanel.add(new JLabel("Simulation Options"), "height=50 gridWidth=5 anchor=CENTER");
    	
    	
    	// ----------------- TOGGLE TRACES -------------------- //
    	
    	myPanel.add(new JLabel(""), "width=10 height=25");
    	traces = new JButton("Traces");							// traces button
    	myPanel.add(traces, "anchor=CENTER width=12 height=25");
    	traces.addActionListener(this);
    	
    	tracesState = new JLabel("Off");						// label to show whether traces are on or off
    	myPanel.add(tracesState, "gridWidth=2 anchor=CENTER");
    	
    	myPanel.add(new JLabel(""), " height=10");
    	
    	
    	
    	// ----------------- EXIT PROGRAM --------------------- //
    	
    	// the idea to use System.exit(0) came from https://stackoverflow.com/questions/23634784/exit-java-applet
    	
    	myPanel.add(new JLabel(""), "gridWidth=5 height=45");	// empty line
    	exit = new JButton("Exit Program");
    	//exit.setBackground(Color.RED);
    	myPanel.add(exit, "gridWidth=5 height=32");
    	
    	
    	exit.addActionListener(this);
    	
    	
	    // ---------- BUTTONS TO RUN PROGRAM COMMANDS --------- //
	    
	  	add(new JLabel("Simulation Options: "), NORTH);	// label
	  	add(new JButton("Stop"), NORTH);				// stop simulation button
	  	add(new JButton("Clear Screen"), NORTH);		// clear screen button
	  	add(new JButton("Stack Balls"), NORTH);			// stack balls button
	  	
	  	
	  	// this code is not redundant; these listeners are for the 4 buttons
	  	// just above this comment
	  	
	  	addActionListeners();		// add action listeners
	    addMouseListeners();		// add mouse listeners
	    
	    add(myPanel, EAST);			// add my panel to the screen on the right side
	    
	    this.resize(WIDTH + PANELWIDTH + 11, HEIGHT + OFFSET); 		// resizing the window
	    
	    resetDisplay();				// reset the display, draw the ground
	    myTree = new bTree();		// initialize an empty bTree
	   
	    rgen = RandomGenerator.getInstance();	// create instance of random generator variable
		rgen.setSeed((long) 424242);							// set seed of random generator
		
		// add an indicator that program is waiting to simulate
		GLabel waiting = new GLabel("Waiting to simulate...", 1015, 625);
		waiting.setColor(Color.RED);
		add(waiting);
	}
}
	
