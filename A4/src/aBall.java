import acm.graphics.*;
import java.awt.Color;

/**
 * Jasper Yun - 260651891 <p>
 * ECSE202 Fall 2019 - Assignment 4 <p>
 * 
 * This program contains the constructor for the simulation and a run() method
 * for simulating projectile motion of the 100 balls. This program does not 
 * take any user input. <p>
 * 
 * This class extends Thread and allows instances of this class and bSim to run
 * at the same time. <p>
 * 
 * aBall is called by the bSim class and contains the code necessary to 
 * make the balls move around on the display canvas. The loop that makes the
 * balls move will break once the balls no longer have sufficient energy. <p>
 * 
 * Much of the code in this class comes from the Bounce.java class from
 * assignment 1.
 * 
 * @author Jasper Yun
 * 
 * @date November 11, 2019
 */


public class aBall extends Thread 
{

// ---------  PARAMETERS: from assignment instructions  --------- //
	
	private static final double g = 9.8;   			// gravitational constant on Earth
	private static final double Pi = 3.1415926535;	// used for converting degrees to radians
	
	// m, k do not change in our program, so change to parameters
	private static final double m = 1.0; 			// mass of ball, given in assignment instructions
	private static final double k = 0.0001; 		// used for air resistance
		
	private static final double TICK = 0.1;			// time increment value	 
	private static final double ETHR = 0.01;		// threshold for termination: if KEx or KEy < ETHR then STOP
	
	volatile boolean RUNNING = true;				// flag representing whether the ball has stopped bouncing
	
	private static final boolean TEST = false; 		// for testing, print info if test true
	private static boolean PRINTTRACES = false;		// flag representing whether the traces of each ball are displayed
	
	private GOval myBall;	// assigning myBall as a GOval object to be used in constructor and run()
	
	// declare variables for the constructor to use
	private double Xi, Yi, V0, theta, bSize, bLoss;
	private Color bColor;
	private bSim link;		// link to bSim to access the methods in bSim
	
	private static final double SCALE = bSim.getScale();	// get the SCALE from bSim class
	
	/**
	* The following code for the constructor is directly from 
	* assignment 2 instructions. The constructor specifies the parameters 
	* for simulation. They are
	*
	* @param Xi double The initial X position of the center of the ball
	* @param Yi double The initial Y position of the center of the ball
	* @param V0 double The initial velocity of the ball at launch
	* @param theta double Launch angle (with the horizontal plane)
	* @param bSize double The radius of the ball in simulation units
	* @param bColor Color The initial color of the ball
	* @param bLoss double Fraction [0,1] of the energy lost on each bounce
	* @param bSim link	Link to the bSim class
	*/
	
	public aBall(double Xi, double Yi, double V0, double theta,
				double bSize, Color bColor, double bLoss, bSim link) 
	{
		this.Xi = Xi; // Get simulation parameters
		this.Yi = Yi;
		this.V0 = V0;
		this.theta = theta;
		this.bSize = bSize;
		this.bColor = bColor;
		this.bLoss = bLoss;
		this.link = link;
		
		// subtract bSize from Xi, Yi to place the center of ball
		myBall = new GOval(Xi - bSize, Yi - bSize, 2 * bSize * SCALE, 2 * bSize * SCALE); 
		myBall.setFilled(true);
		myBall.setFillColor(bColor);
		myBall.setColor(Color.BLACK);
		
	} 
	
	
	
	/**
	 * Gets the instance of aBall.
	 * The code comes from the assignment instructions.
	 * 
	 * @param void
	 * @return aBall
	 */
	GOval getBall()  { return (myBall); }
	
	
	/**
	 * Gets the ball size of the aBall object.
	 * @return double bSize
	 */
	double getBSize() { return bSize; }
	
	
	/**
	 * Sets the ball size of the particular aBall object.
	 * @param arg - <code>double</code> new ball size
	 */
	void setBSize0() { this.bSize = 0; }
	
	
	/**
	 * Sets the color and border of existing aBall objects
	 * to white, and sets the size to 0. <p>
	 * 
	 * This is used in the ball stacking method in the
	 * <code>bTree</code> class to "delete" existing balls.
	 * 
	 *  @param void
	 *  @return void
	 */
	void setColorOff() {
		
		myBall.setColor(Color.WHITE);
		myBall.setFillColor(Color.WHITE);
		myBall.setSize(0,0);
		//myBall.setVisible(false);
		
		
		//link.remove(myBall);
	}
	
	/**
	 * Gets the flag indicating whether the ball
	 * is bouncing.
	 * 
	 * @return boolean True if ball is still bouncing
	 */
	boolean isRunning() { return RUNNING; }
	
	
	/**
	 * Sets the boolean instance variable to the state
	 * specified by the argument. <p>
	 * 
	 * @param state <code>boolean</code> Change <code>RUNNING</code> to this state
	 */
	void setRunning(boolean state) { RUNNING = state; }
	
	/**
	 * Sets the location of the ball to some point on
	 * the screen. The location of the ball is with respect
	 * to the center of the ball.
	 * 
	 * @param x double - x coordinate in simulation (MKS) units
	 * @param y double - y coordinate in simulation (MKS) units
	 */
	void moveTo(double x, double y)
	{
		// converting arguments to screen units (pixels)
		int ScrX = (int) ((x) * bSim.getScale()); 
		int ScrY = (int) (bSim.getHEIGHT() - (y + bSize) * bSim.getScale());
		
		myBall.setLocation(ScrX, ScrY);	// set location
	}
	
	
	/**
	 * Adds trace points to plot the trajectory of each ball
	 * in the simulation. The trace points match the color of 
	 * each ball. <p>
	 * 
	 * @param x double 	X position of the ball in meters
	 * @param y double	Y position of the ball in meters
	 */
	private void traces(double x, double y)
	{
		// convert the arguments x, y to pixel units
		double ScrX = (x) * bSim.getScale();
		double ScrY = bSim.getHEIGHT() - (y+bSize) * bSim.getScale();
		
		// add trace points -- we set radius 2 so that the color is visible
		GOval point = new GOval(ScrX, ScrY, 1, 1);
		point.setColor(this.bColor);
		point.setFilled(true);
		link.add(point);
		
	}
	
	
	
	/**
	 * <code>toString()</code> method defines how a ball object should
	 * be printed. This was used during testing.
	 * 
	 * @param void
	 * @return String String containing the ball size and color.
	 */
	public String toString()
	{
		return "Ball size: " + bSize + ", Color: " + bColor;
	}
	
	/**
	* The run method implements the simulation loop from Assignment 1.
	* Once the start method is called on the aBall instance, the
	* code in the run method is executed concurrently with the main
	* program.
	* 
	* @param void
	* @return void
	*/
	
	public void run() 
	{	
		// ---------  INITIALIZE VARIABLES:  -------------------- //
		
		double V0x = V0 * Math.cos(theta * Pi/180); 	// X component of initial velocity V0
		double V0y = V0 * Math.sin(theta * Pi/180); 	// Y component of initial velocity V0
		double Vt = m * g / (4 * Pi * bSize * bSize * k); // terminal velocity of ball
		
		
		double X = Xi; 			// X begins at Xi
		double Y = bSize; 		// Y begins at Yi
		double Xoff = Xi; 		// offset of X which gets changed every bounce
		
		double Xlast = Xoff; 	// X at end of iteration of while loop
		double Ylast = Y; 		// Y at end of iteration of while loop
		
		double Vx = V0x; 		// velocity in x direction will be same as initial x velocity at t = 0 
		double Vy = V0y; 		// velocity in y direction will be same as initial y velocity at t = 0
		
		double KEx = 0.5 * Vx * Vx; // kinetic energies as given by assignment instructions
		double KEy = 0.5 * Vy * Vy;
		
		double t = 0;			// start time t=0
		double totalTime = 0;	// variable to track total time elapsed, used in print statement of testing
		
		
		// we set prevTotalEnergy to be the max value a double can contain so that the simulation MUST run
		// the idea comes from Nicholas Dahdah, fellow student at McGill University
		double prevTotalEnergy = Double.MAX_VALUE; 	// variable to track total energy of balls
		double totalEnergy = 0; 					// variable to track current total energy of balls
		
		
		
		// ---------  MAIN LOOP:  ------------------------------- //
		// loop for ball simulation: runs while RUNNING is true
		while (RUNNING)
		{
			X = V0x * Vt / g * (1 - Math.exp(-g * t / Vt)); 							// update X position
			Y = bSize + Vt / g * (V0y + Vt) * (1 - Math.exp(-g * t / Vt)) - Vt * t; 	// update Y position
			
			 //  we need the Vx, Vy to be updated before we determine collision so that if there
			 // is a collision, it is using the correct values for Vx, Vy in the calculation of
			 //  kinetic energies. Otherwise, the ball rolls at the end for a long time.
			
			Vx = (X - Xlast)/TICK; // update (estimate) x, y velocities based on displacement over time difference
			Vy = (Y - Ylast)/TICK; 
			
			
			Xlast = X; // let Xlast be the current X coordinate (metres) of the ball
			Ylast = Y; // let Ylast be the current Y coordinate (metres) of the ball
			
			prevTotalEnergy = KEx + KEy; // calculate the 'previous' total energy
			
		// ---------  COLLISION DETECTION:  ----------------------- //
			if(Y<=bSize && Vy<0)
			{
				// calculate new kinetic energies in x, y directions after collision
				KEx = 0.5 * Vx * Vx * (1 - bLoss); 
				KEy = 0.5 * Vy * Vy * (1 - bLoss);
				
				
				// we need to check if the condition to break is true only after
				// the ball bounces, so we recalculate the totalEnergy only after
				// a bounce occurs, i.e. inside this if statement
				totalEnergy = KEx + KEy;
				
				
				// the two conditions given in the assignment instructions are for the simulation
				// to continue. The condition needed to end the simulation is the logical not of the 
				// two conditions given in the assignment instructions. We apply de Morgan's laws and 
				// obtain the condition below: 
				if ((KEx + KEy) <= ETHR || prevTotalEnergy <= totalEnergy) 
				{	
					RUNNING = false;		// set flag to false to indicate ball has stopped
					break; 
				}
				
				V0x = Math.sqrt(2 * KEx); 	// calculate new initial x, y velocities from KEx, KEy
				V0y = Math.sqrt(2 * KEy);
				
				if (theta > 90) V0x = -V0x;		// balls moving to left, so velocity is negative
					
				t = 0; 			// reset time to 0
				Y = bSize;  	// ball is on ground, so Y = bSize
				Xoff += X;  	// add current X to Xoff
				X = 0; 			// reset X to 0 -- in position of ball, use Xoff + X
				Xlast = X;  	// Reset last X, Y positions
				Ylast = Y;
				
			}
			
			// ---------  DISPLAY UPDATE:  ---------------------- //
			int ScrX = (int) ((X + Xoff - bSize) * SCALE); 		 // changing positions to screen coordinates
			int ScrY = (int) (600 - (Y + bSize) * SCALE); 
			
			// add traces to canvas
			if (link.getPRINTTRACES()) traces(X + Xoff, Y);
			
			myBall.setLocation(ScrX, ScrY); // update location of ball
			
			
			if (TEST) // printing information for debugging
			{
				System.out.printf("t: %.2f X: %.2f Y: %.2f Vx: %.2f Vy: %.2f\n", totalTime, Xoff + X, Y, Vx, Vy);
				
			}

			t += TICK; 				// increment time by TICK
			totalTime += TICK;		// increment total time by TICK
			

			try 
			{ 	
				Thread.sleep(50);						// pause for 50 milliseconds
			}											// this code comes from assignment instructions
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
			
		}		
	}


		
} // class




