
/**
 * Jasper Yun - 260651891 <p>
 * ECSE202 Fall 2019 - Assignment 4 <p>
 * 
 * This class implements a non-recursive binary tree. <p>
 * 
 * The code in this class is heavily borrowed from the 
 * example class posted by Prof. Ferrie on myCourses for
 * ECSE202, Fall 2019. I have modified it to be able
 * to store aBall objects instead of integers. <p>
 * 
 * Original code modified by Jasper Yun for use with aBall
 * objects. <p>
 * 
 * @author ferrie
 * @author Jasper Yun
 * @date November 11, 2019
 *
 */

public class bTree 
{
	
	// Instance variables
	
    private static bNode root = null;		// initialize empty bTree
    
    private double lastSize = 0;	// initialize lastSize = 0 to ensure that first ball gets placed
    private double X=0, Y=0;		// for stackBalls method; 
    private double DELTASIZE = 0.1;	// threshold to determine whether to start new stack (sim units - meters) 
    
    // used in isRunning() method; indicates whether any balls in bTree are running
    private static boolean treeRunning = false;	

	
    
    /**
     * A simple bNode class for use by bTree.  The "payload" can be
     * modified accordingly to support any object type. <p>
     * 
     * This code has been modified by Jasper Yun to store a reference
     * to an object of type aBall. <p>
     * 
     * @author ferrie
     * 
     */

    class bNode 
    {           		// we need each node to store an aBall object
        aBall ball;     // stores the reference to the aBall
        bNode left;     // reference to left child node
        bNode right;    // reference to right child node
    }

    
    
    /**
     * Gets the root node of the bTree.
     * 
     * @return root - <code>bNode</code> root node of the bTree
     */
    bNode getRoot() { return root; }
    
    /**
     * addNode method - adds a new node by descending to the leaf node
     *                  using a while loop in place of recursion.  Ugly,
     *                  yet easy to understand.
     */
	
    public void addNode(aBall ball) 
    {	
		bNode current;
		
		double bSize = ball.getBSize();		// get the size of the ball being added

        // Empty tree
		if (root == null) root = makeNode(ball);
		
        // If not empty, descend to the leaf node according to
        // the size of the input ball.
        else 
        {
			current = root;
            while (true) 
            {
                if (bSize < current.ball.getBSize()) 		// branch left or right based on ball size
                {
                    // If new ball is smaller than current ball, branch left
                    if (current.left == null) {				// leaf node
						current.left = makeNode(ball);		// attach new node here
						break;
					}
					else current = current.left;			// otherwise, keep traversing
				}
                
                else 
                {
                    // If new ball is larger than current ball, branch right
					
                    if (current.right == null) {  			// leaf node
                        current.right = makeNode(ball);		// attach
                        break;
                    }
                    else current = current.right;			// otherwise keep traversing
				}
			}
		}
		
	}
    
	
    /**
     * makeNode <p>
     * 
     * Creates a single instance of a bNode
     * 
     * @param	aBall ball   Ball to be added
     * @return  bNode node 	 Node created
     */
	
    bNode makeNode(aBall ball) 
    {
		bNode node = new bNode();				// create new node object
		node.ball = ball;						// store reference to the ball
		node.left = null;						// set both successors to null
		node.right = null;
		return node;							// return handle to new object
	}
	
	
    /**
     * stackBalls method - inorder traversal via call to recursive method
     * to stack the balls in order of ball size.
     */
	
    void stackBalls() {
    	X = 0;				// initialize X, Y, lastSize = 0 for each time we call stackBalls()
    	Y = 0;
    	lastSize = 0;
    	if (root != null)	// only stack the balls if the root node is not null
    		traverse_Stacking(root);
    }
    
    /**
     * traverse_inorder method - recursive method to traverse the
     * bTree. Used to stack the balls. <p>
     * 
     * The method is <code>private</code> to hide complexity from
     * the user. <p>
     * 
     * @param bNode root	root node of the bTree
     */
    
    private void traverse_Stacking(bNode root) 
    {
    	if (root.left != null) traverse_Stacking(root.left);
   
    	// process for stacking balls:
    	// 		1. Get size of ball at current node.
    	// 		2. Update values of X and Y to determine where to place it.
    	// 		3. moveTo(X,Y) to place the ball there
    	
    	if (root.ball != null) {
    	//	if (root.ball.getBSize() == 0) root.ball.setColorOff();		// if ball size = 0, then the ball is "deleted"
    																	// then "turn off" each ball and hide it from the canvas
    		
    		//else {
		    	double currentSize = root.ball.getBSize();				// for balls that we need to stack
		    	
		    	if (currentSize - lastSize > DELTASIZE)	// start new stack
		    	{
		    		X += 2.0 * lastSize;	// move X to right by 2*currentSize
		    		Y = currentSize;		// on new stack, the new Y is height of the center of the ball
		    		root.ball.moveTo(X, Y);	// move ball
		    		
		    		
		    		lastSize = currentSize;	// reassign the last size to be the size of new ball
		    	}
		    	else
		    	{
		    		Y += 2.0 * currentSize;	// make new Y on top of the old ball
		    		root.ball.moveTo(X, Y);	// move ball
		    		lastSize = currentSize;	// reassign last size to be new ball size
		    	}
    		}
    	//}
    	
    	if (root.right != null) traverse_Stacking(root.right);
    }
   
    
    /**
     * Checks the status of the balls in the bTree by calling 
     * in order traversal method. Returns true if there are
     * any balls still bouncing. Returns false otherwise. <p>
     * 
     * @return treeRunning - <code>boolean</code> flag indicating whether any balls are still bouncing
     */
    
    boolean isRunning()
    {
    	
    	treeRunning = false;
    	
    	if (root != null)			// check only if we have non-empty tree
    		traverse_status(root);	// call to recursive method
    	
    	return treeRunning;
    }
    
    
    
    
    /**
     * Checks whether the simulation is still running by
     * checking the bTree recursively. The tree sets <code>treeRunning</code>
     * to true if any of the balls are still running, and false only when
     * all of the balls have stopped running. <p>
     * 
     * @param root - <code>bNode</code> root node of bTree
     * @return void
     */
    
    private static void traverse_status(bNode root) {
    	
    	if (root.left != null) traverse_status(root.left);
    	
    	// we check if root.ball != null to prevent errors (NullPointerException)
    	if (root.ball != null && root.ball.isRunning() == true) treeRunning = true;
    	
    	if (root.right != null) traverse_status(root.right);
    
    }
    
    
    
    /**
     * Adds balls back to the canvas by calling a method 
     * which will traverse the tree. This hides complexity from the user.<p>
     * 
     * The method takes an argument of a link to <code>bSim</code>
     * which allows it to add the balls back to the canvas. <p>
     * 
     * @param link - <code>bSim</code> link to <code>bSim</code> class
     * @return void
     */
    void addBallBack(bSim link) {
    	if (root != null)					// run only if we have non-empty bTree
    		traverse_addBalls(root, link);	// call to recursive method
    	
    }
    
    /**
     * recursive traversal of the bTree to add balls back to canvas.<p> 
     * 
     * @param root <code>bNode</code> Root node of the bTree
     * @param link <code>bSim</code> Link to <code>bSim</code> class
     */
    
    private void traverse_addBalls(bNode root, bSim link) {
    	
    	if (root.ball != null)					// add ball to bSim canvas if the ball is not null
    		link.add(root.ball.getBall());		// add the ball at the current node
    	
    	if (root.left != null) traverse_addBalls(root.left, link);
    	if (root.right != null) traverse_addBalls(root.right, link);
    	
    	
    }
    
    /**
     * Deletes the bTree by setting <code>root = null</code>, only
     * if the tree is nonempty to prevent <code>NullPointerException</code>. <p>
     * 
     * @param void
     * @return void
     */
    void clearAll() {
    	if (root != null && root.ball != null) {		// run on non-empty bTree only
    		root = null;	// clear the bTree by setting root to null
    	}
    }
    
   
    /**
     * This method was for testing the tree. It calls a recursive
     * method that traverses the tree to print each node.
     * 
     */
    void printTree() {
    	if (root != null)
    		PNinorder(root);
    }
    
    /**
     * For testing. The method recursively traverses the bTree in order
     * and prints each node. <p>
     * 
     * @param root - <code>bNode</code> Root node of the bTree
     */
    private void PNinorder(bNode root)
    {
    	if (root.left != null) PNinorder(root.left);
    	
    	// inorder traversal
    	System.out.print("Current node: ");			// for testing,
    	System.out.println(root);						// print current node
    	if (root.right != null) PNinorder(root.right);
    }
    
    
    /**
     * Pauses the display by setting <code>RUNNING = false</code> for each ball
     * in the tree. This is done by calling a recursive method to hide complexity
     * from the user.<p>
     * 
     * @param void
     * @return void
     */
    void pause() {
    	if (root.ball != null)	// prevent errors if the bTree is empty
    		pauseTree(root);	// call to recursive method
    }
    
    /**
     * Traverses the tree postorder and sets <code>RUNNING = false</code>
     * for every ball in the tree. <p>
     * 
     * @param root - <code>bNode</code> Root node of the bTree
     * @return void
     */
    private static void pauseTree(bNode root)
    {
    	if (root.left != null) pauseTree(root.left);
    	if (root.right!= null) pauseTree(root.right);
    	root.ball.setRunning(false);	// set RUNNING = false to pause the ball
    }
    
}


