import javax.swing.*;

import acm.gui.*;
import acm.program.*;
import acm.graphics.*;


/**
 * Jasper Yun - 260651891 <p>
 * ECSE202 Fall 2019 - Assignment 4 <p>
 * 
 * This class creates a few dropdown menus for the GUI.
 * I put the code for these dropdown menus here because
 * I implemented all of the necessary commands to run the program
 * using JButtons, thus the dropdowns were purely for esthetics.
 * 
 * @author Jasper Yun
 * 
 * @date November 11, 2019
 */

@SuppressWarnings("serial")
public class gui extends GraphicsProgram {
	
	/**
	 * Creates a dropdown menu containing the following options:
	 * <li>File
	 * <li>Open...
	 * <li>Save
	 * <li>Save As
	 * <li>Exit
	 * </li>
	 * @return dropdown2 - <code>JComboBox</code> dropdown menu
	 */
	
	static JComboBox dropdown2()
	{
		JComboBox dropdown2 = new JComboBox(); 
		dropdown2.addItem("File"); 
		dropdown2.addItem("Open..."); 
		dropdown2.addItem("Save"); 
		dropdown2.addItem("Save As"); 
		dropdown2.addItem("Exit");
		dropdown2.setEditable(false);
		return dropdown2;
	}
	
	/**
	 * Creates a dropdown menu containing the following options:
	 * <li>Edit
	 * <li>Undo
	 * <li>Redo
	 * <li>Copy
	 * <li>Paste
	 * </li>
	 * @return dropdown3 - <code>JComboBox</code> dropdown menu
	 */
	static JComboBox dropdown3()
	{
		JComboBox dropdown3 = new JComboBox(); 
		dropdown3.addItem("Edit"); 
		dropdown3.addItem("Undo"); 
		dropdown3.addItem("Redo"); 
		dropdown3.addItem("Copy");
		dropdown3.addItem("Paste");
		dropdown3.setEditable(false);
		return dropdown3;
	}
	
	
	/**
	 * Creates a dropdown menu containing the following options:
	 * <li>Help
	 * <li>Report Bug
	 * <li>Contact Developer
	 * <li>Check for Updates
	 * </li>
	 * @return dropdown4 - <code>JComboBox</code> dropdown menu
	 */
	static JComboBox dropdown4()
	{
		JComboBox dropdown4 = new JComboBox(); 
		dropdown4.addItem("Help"); 
		dropdown4.addItem("Report Bug"); 
		dropdown4.addItem("Contact Developer"); 
		dropdown4.addItem("Check for Updates"); 
		dropdown4.setEditable(false);
		return dropdown4;
	}
	
	
}
