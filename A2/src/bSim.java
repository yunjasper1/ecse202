import acm.program.*;
import acm.util.RandomGenerator;
import java.awt.Color;
import acm.graphics.*;


/**
 * Jasper Yun - 260651891 <p>
 * ECSE202 Fall 2019 - Assignment 2 <p>
 * 
 * This program contains the code to set up the graphics environment for
 * simulating projectile motion of 100 balls. This program does not take user
 * input. It generates parameters for the aBall constructor in a
 * pseudorandom manner. <p>
 * 
 * Once run, this class will create a display window of 1200 x 800 pixels.
 * The balls will begin on the ground in the middle of the screen. The 
 * simulation will end when the balls no longer have sufficient energy. <p>
 * 
 * @author Jasper Yun
 * 
 * @date October 14, 2019
 */


public class bSim extends GraphicsProgram 
{
	
// ---------  PARAMETERS: from assignment instructions  --------- //
	
	// display window parameters in pixels
	private static final int WIDTH = 1200; 
	private static final int HEIGHT = 600; 			// distance from top of screen to ground plane
	private static final int OFFSET = 200; 			// distance from ground to bottom of screen
	public static final double SCALE = HEIGHT/100;	// scale factor in pixels/metre
	
	private static final int NUMBALLS = 100;		// number of balls to simulate
	
	// ranges for constructor parameters
	private static final double MINSIZE = 1.0; 		// minimum ball radius in meters
	private static final double MAXSIZE = 10.0;		// maximum ball radius in meters
	
	private static final double EMIN = 0.1;			// minimum energy loss coefficient
	private static final double EMAX = 0.6;			// maximum energy loss coefficient
	
	private static final double V0MIN = 40.0;		// minimum initial velocity (m/s)
	private static final double V0MAX = 50.0;		// maximum initial velocity (m/s)
	
	private static final double ThetaMIN = 80.0;	// minimum launch angle (degrees)
	private static final double ThetaMAX = 100.0;   // maximum launch angle (degrees)
	
	
	/**
	 * Creates the graphics environment and resizes the display canvas window.
	 * The method also uses RandomGenerator rgen to pseudorandomly generate 
	 * values for the parameters of the aBall constructor. These values
	 * are passed into the aBall constructor 100 times to create 100 balls
	 * for the simulation.
	 * 
	 * @param void
	 * @return void
	 */
	
	public void run() 
	{
		this.resize(WIDTH, HEIGHT + OFFSET); 		// resizing the window
		
	// -----  GRAPHICS: ground and display window resizing  ----- //
		
		// draw graphics display and ball:
		GRect ground = new GRect(0,HEIGHT,WIDTH,3);
		ground.setFilled(true);
		ground.setFillColor(Color.BLACK);
		
		// add ground and ball to canvas
		add(ground);
		
		RandomGenerator rgen = RandomGenerator.getInstance();	// create instance of random generator variable
		rgen.setSeed((long) 0.12345);							// set seed of random generator
		
		// declare variables for the constructor
		double Xi, Yi, V0, theta, bSize, bLoss;
		Color bColor;
		
		
		// generating parameters for aBall constructor -- this is my code
		for (int i = 0; i < NUMBALLS; i++)					// for loop runs 100 times to create 100 different balls
		{
			bSize = rgen.nextDouble(MINSIZE, MAXSIZE);		// generate random values for the arguments of the aBall constructor
			bColor = rgen.nextColor();						// note that we generate in the order specified by Prof. Ferrie
			bLoss = rgen.nextDouble(EMIN, EMAX);			
			V0 = rgen.nextDouble(V0MIN, V0MAX);				
			theta = rgen.nextDouble(ThetaMIN, ThetaMAX);	
			
			Xi = (WIDTH / 2) / SCALE; 	    // all balls begin their adventure at the middle of the screen
			Yi = bSize; 					// starting position of balls on screen
			
			// creating ball
			aBall newBall = new aBall(Xi, Yi, V0, theta, bSize, bColor, bLoss);
			add(newBall.getBall());	// add ball to screen
			newBall.start(); 		// start the thread of the ball created
			 	
		}
		
		
		
		/*			// for verifying A1-1, A1-2
		bSize = 1;
		Xi = 95;
		Yi = bSize;
		aBall newBall = new aBall(Xi, Yi, 40, 95, 1, Color.RED, 0.4);
		add(newBall.getBall());
		newBall.start();
		*/
		
	}
		
}

